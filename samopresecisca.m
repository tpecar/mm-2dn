function [P, T, A] = samopresecisca(p, pdot, a, b, h)
  % poisce samopresecisca parametrizirane krivulje
  % VHOD:
  %   p - kazalec na funkcijo parametrizirane krivulje na ravnini
  %       oblike p(t), ki za podanih n vrednosti neodvisnih spremenljivk
  %       t vrne koordinate tocke v obliki A_2xn
  %   pdot - kazalec na funkcijo odvoda parametrizirane krivulje -
  %          za podano vrednost neodvisne spremenljivke t vrne
  %          rezultat Jacobijeve matrike za dan vhod
  %             ce se omejimo na krivulje na ravnini, mora biti
  %             Jacobijeva matrika biti oblike 2x2
  %             [odvod po x,t  odvod po x,s]
  %             [odvod po y,t  odvod po y,s]
  %   a, b - krajisci intervala, iz katerega jemljemo vrednosti
  %          neodvisne spremenljivke pri tvorbi koordinat lomljenke
  %          za grobo aproksimacijo presecisc
  %   h - je korak s katerim jemljemo vrednosti iz intervala [a,b]
  %
  % IZHOD:
  %   P - matrika koordinat samopresecisc [oblike 2xk], ki vrne koordinate
  %       samopresecisc, urejenih po narascajocih vrednostih parametra t
  %   T - matrika parametrov t presecisc, ki pripadajo koordinatam v P
  %   A - matrika prametrov priblizkov t ter s, ki so se uporabili za zacetno
  %       vrednost pri Newtonovi metodi (gre za matriko vektorjev oblike [t;s])
  %       - to je predvsem za namen izrisa
  %
  % pri razvoju sem uporabil implementacije resevanj posameznih podproblemov,
  % ki smo jih resevali na vajah
  %   presecisca.m
  %   presecisce.m
  %   newton.m

  % tvorimo matriko neodvisnih spremenljivk t,s
  t = a:h:b;
  % stevilo tock na lomljenki
  tlen = length(t);
  
  % tvorimo matriko preracunanih vrednosti funkcije p
  xy = p(t);
  
  % poiscemo najvecjo dolzino v x ter y smeri - ce imamo dve tocki, katerih
  % medsebojna razlika je vecja dvakratnika od tega, potem se daljici njune
  % lomljenke
  % morejo niti stikati in je brez pomena sploh resevati sistem
  % da se uporabiti tudi manjsi faktor - za 8. formulo je recimo dovolj ze 1.1,
  % vendar pa je to mocno odvisno od primera, prav tako to ne doprinese
  % vecjih pospesitev - kvecjemu za 1 s, ker jih je treba preveriti vec
  % v gosto posejanem obmocju
  max_raz = 2*(max(abs(xy(:,1:end-1)-xy(:,2:end))')');

  % matrika koordinat natancnih izracunov
  P = [];
  % matrika parametrov za koordinate
  T = [];
  % matrika parametrov priblizkov
  A = [];
  
  %poiscemo presecisca vseh parov daljic
  for i = 1:(tlen-1) % zanka po daljicah iz lomljenke (A)
    % glede na to da iscemo kombinacije na isti lomljenki ter da vrstni red
    % daljic ni pomemben, gremo od indeksa i naprej
    % hkrati ob tem
    % izlocimo robne primere, ki se pojavijo pri samopreseciscih
    % zaradi aproksimacije z lomljenkami:
    %   sosednji daljici nikoli ne moreta tvoriti samo presecisce
    %   (v osnovi izlocimo tudi s pogojem [0,1))
    %
    %   prav tako ne sme sama premica tvoriti presecisce s sabo
    %   (prav tako bi se izlocilo zaradi [0,1))
    % posledicno morata biti indeksa gledanih (zacetnih) tock
    % vsaj za 2 razlicni
    for j = i+2:(tlen-1) % zanka po daljicah iz iste lomljenka (B)
      if(abs(xy(:,i)-xy(:,j))<=max_raz)
        % pridobimo faktor odmika
        fakt = presecisce(xy(:,[i i+1]), xy(:,[j j+1]));
        % Ce se daljici sekata, poiscemo natancnejsi priblizek z Newtonovo metodo
        if(length(fakt)>0)
        
          % zacetnima t ter s pristejemo zmnozek faktorja odmika od t,s s
          % korakom intervala h
          % ta zveza velja, ker mi pri iskanju presecisc uporabimo daljici,
          % katerih tocki sta bili izracunani ravno za t(i), t(i)+h, s tem ko
          % resujemo sistem presecisc premic z zacetno tocko p(t(i)), pa
          % bo faktor, ki mnozi smerni vektor
          % (dobljen kot razlika zacetne ter koncne tocke daljice)
          % v istem razmerju s faktorjem, ki mnozi h
          faktval = [t(i); t(j)]+h.*fakt;
          % damo kar izracun za obe zacetni tocki t in s
          A = [A faktval];
          
          % kot funkcijo podamo funkcijo krivulje, le da med sabo odstejemo
          % vrednosti za t in s, saj mi resujemo sistem f(t)-f(s)=0
          % od izhoda metode vzamemo priblizek parametra t - lahko bi tudi s,
          % saj mi v tej tocki ze izracunamo koordinate, glavno je le, da te
          % grejo po narascajocih vrednostih parametra t, kar imamo zagotovljeno
          % ze zaradi zanke
          T = [T, newton(@(x) p(x(1))-p(x(2)), @(x) [pdot(x(1)), -pdot(x(2))], faktval, 1e-10, 100)(1)];
          
          P = [P, p(T(end))];
        end
      end
    end
  end
endfunction
% TESTI
% [-x.^3+x; x.^2] je parametricna funkcija s samopreseciscem v 1
%!test
%! p = @(x) [-x.^3+x; x.^2];
%! pdot = @(x) [-3*x.^2+1; 2*x];
%! a=-2;
%! b=2;
%! h=0.1;
%! P = samopresecisca(p, pdot, a, b, h);
%! assert(P,[0;1],1e-5)

% parametricna enaka zgornji, le da z odmikom
%!test
%! p = @(x) [-x.^3+x+5; x.^2+5];
%! pdot = @(x) [-3*x.^2+1; 2*x];
%! a=-2;
%! b=2;
%! h=0.1;
%! P = samopresecisca(p, pdot, a, b, h);
%! assert(P,[5;6],1e-5)

% prva parametricna funkcija z zamenjanimi enacbami
%!test
%! p = @(x) [x.^2; -x.^3+x];
%! pdot = @(x) [2*x; -3*x.^2+1];
%! a=-2;
%! b=2;
%! h=0.1;
%! P = samopresecisca(p, pdot, a, b, h);
%! assert(P,[1;0],1e-5)

% zakljucena Lissajousova krivulja z edinim samopreseciscem v 0,0
%!test
%! p = @(x) [cos(x); sin(x*2)];
%! pdot = @(x) [-sin(x); 2*cos(2*x)];
%! a=0;
%! b=2*pi;
%! h=pi/10;
%! P = samopresecisca(p, pdot, a, b, h);
%! assert(P,[0;0],1e-5)

% DEMO

% dolocitev presecisc za funkcijo, ki je zahtevana glede na vpisno stevilko
% (8. funkcija)
%!demo
%! p= @(t) [9*cos(t) + 2*cos(9*t); 9*sin(t) - 2*sin(9*t) - 2*t];
%! pdot = @(t) [(-18*sin(9*t))-9*sin(t); (-18*cos(9*t))+9*cos(t)-2];
%! a=0;
%! b=2*pi;
%! h=0.05;
%! figure 1
%!  [P, T, A] = samopresecisca(p, pdot, a, b, h);
%!  hold on
%!  
%!  % izrisemo samo krivuljo - uporabimo 10 krat manjsi interval od podanega
%!  p_val = p(a:h./10:b);
%!  plot(p_val(1,:), p_val(2,:));
%!  
%!  % izracunamo koordinate prvo za vse priblizke t, nato se za s
%!  A_val = [p(A(1,:)) p(A(2,:))];
%!  plot(A_val(1,:), A_val(2,:),'go');
%!  plot(P(1,:), P(2,:), 'r*');
%!  
%!  axis on
%!  grid on
%!  hold off
%! title("Samopresecisca 8. parametricne krivulje iz navodil");

% dolocitev presecisc Lissajousove krivulje a=5, b=6
%!demo
%! p = @(x) [cos(5 * x); sin(6 * x)];
%! pdot = @(x) [-5*sin(5*x); 6*cos(6*x)];
%! a=0;
%! b=2*pi;
%! h=pi/60;
%! figure 2
%!  [P, T, A] = samopresecisca(p, pdot, a, b, h);
%!  hold on
%!  
%!  % izrisemo samo krivuljo - uporabimo 10 krat manjsi interval od podanega
%!  p_val = p(a:h./10:b);
%!  plot(p_val(1,:), p_val(2,:));
%!  
%!  % izracunamo koordinate prvo za vse priblizke t, nato se za s
%!  A_val = [p(A(1,:)) p(A(2,:))];
%!  plot(A_val(1,:), A_val(2,:),'go');
%!  plot(P(1,:), P(2,:), 'r*');
%!  
%!  axis on
%!  grid on
%! title("Samopresecisca Lissajousove krivulje a=5, b=6");

function T = presecisce(A, B)
  %A1A2 in B1B2 v ravnini.
  %A = [A1, A2], A1 in A2 sta krajevna vektorja tock na prvi daljici.
  %B = [B1, B2], B1 in B2 sta krajevna vektorja tock na drugi daljici.
  %T = parameter presecisca

  %Resimo izpeljani sistem enacb za parametra presecisca obeh premic nosilk.
  %... vrnemo faktor, s katerim smo mnozili smerna vektorja, da smo
  %    dobili isto tocko
  T = [A(:, 2) - A(:, 1), -(B(:, 2) - B(:, 1))]\(B(:, 1) - A(:, 1));
  %Preverimo, ce sta parametra znotraj intervala [0, 1] in...
  % mi lahko namrec se vedno dobimo resitev, vendar pa ce bosta
  % koeficienta bodisi negativna, bodisi vecja od 1, to pomeni, da
  % se premici (ki v grobem aproksimirata krivulji) sekata IZVEN
  % obmocja, skozi katere krivulji potujeta
  % ti namrec potujeta le znotraj obmocja A1A2, B1B2
  
  % PRI TEM JE POTREBNO PAZITI da mi moramo obravnavati interval [0,1), torej
  % brez 1, ker drugace bi zaradi prekrivanja resitev veckrat lahko nasli
  % isto presecisce!
  if(!(T(1) < 1 && T(2) < 1 && T(1) >= 0 && T(2) >= 0))
    T=[];
  end
endfunction

function [x0, k] = newton(F, JF, x0, tol, maxit)
  %[x, k] = newton(F, JF, x0, tol, maxit) poisce priblizek 
  %x za resitev enacbe F(x) = 0 z Newtonovo metodo.
  %(k je stevilo korakov, ki jih metoda porabi.)
  %F... funkcija
  %JF... Jacobijeva matrika F
  %x0... zacetni priblizek
  %tol... zahtevana natancnost
  %maxit... najvecje stevilo iteracij

  for k = 1:maxit
    %Izvedemo en korak Newtonove metode...
    px = x0; % prejsnji x0 shranimo
    x0 = x0 - feval(JF, x0)\feval(F, x0);
    %... in testiramo, ce je metoda ze 'konvergirala'.
    if(norm(x0-px) < tol)
      break;
    end
  end

  %Izpisemo opozorilo v primeru, da zadnji priblizek ni znotraj tolerancnega obmocja.
  if(k == maxit)
    printf("POZOR: metoda ni konvergirala! [%d izven tolerance %d]\n",norm(x0-px), tol)
  end
endfunction