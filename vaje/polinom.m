function p = polinom(p0, d0, p1, d1)
%p = polinom(p0, d0, p1, d1) vrne koeficiente 
%polinoma p stopnje 3 s predpisanimi vrednostmi 
%in odvodi v krajiscih intervala [0, 1].

%matrika sistema
A = [0 0 0 1; 1 1 1 1; 0 0 1 0; 3 2 1 0];
%desna stran sistema
b = [p0; p1; d0; d1];
%resimo sistem
p = A\b;