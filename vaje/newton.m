function [x, k] = newton(F, JF, x0, tol, maxit)
%[x, k] = newton(F, JF, x0, tol, maxit) poisce priblizek 
%x za resitev enacbe F(x) = 0 z Newtonovo metodo.
%(k je stevilo korakov, ki jih metoda porabi.)
%F... funkcija
%JF... Jacobijeva matrika F
%x0... zacetni priblizek
%tol... zahtevana natancnost
%maxit... najvecje stevilo iteracij

for k = 1:maxit
	%Izvedemo en korak Newtonove metode...
	x = x0 - feval(JF, x0)\feval(F, x0);
	%... in testiramo, ce je metoda ze 'konvergirala'.
	if(norm(x - x0) < tol)
		break;
	end
	x0 = x;
end

%Izpisemo opozorilo v primeru, da zadnji priblizek ni znotraj tolerancnega obmocja.
if(k == maxit)
	disp("Warning: The method did not converge after maxit iterations.")
end