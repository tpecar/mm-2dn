function zlepek(A)
%zlepek(A) narise gladek kubicni zlepek
%skozi tocke A = [A1, A2, ..., An].

%shranimo velikost A
[dim, n] = size(A);

%oznacimo tocke, skozi katere gre zlepek
if dim == 2
	plot(A(1, :), A(2, :), "ko");
else
	plot3(A(1, :), A(2, :), A(3, :), "ko");
end

%Poskrbimo, da vse naslednje grafe risemo na isto sliko
hold on;

%pripravimo 100 vrednosti med 0 in 1
t = linspace(0, 1);

for k = 1:(n-1)
	%Izracunamo koeficiente polinomov, ki parametrizirajo zlepek med Ak in Ak+1.
	%Za tangentni vektor v Ak izberemo kar A(k+1) - A(k-1).
	%Zacetna in koncna tocka sta posebni...
	if k == 1
		p = polinom(A(:, k)', zeros(1, dim), A(:, k+1)', A(:, k+2)' - A(:, k)');
	elseif k == n-1;
		p = polinom(A(:, k)', A(:, k+1)' - A(:, k-1)', A(:, k+1)', zeros(1, dim));
	else
		p = polinom(A(:, k)', A(:, k+1)' - A(:, k-1)', A(:, k+1)', A(:, k+2)' - A(:, k)');
	end
	%Narizemo del zlepka med Ak in Ak+1.
	if dim == 2
		plot(polyval(p(:, 1), t), polyval(p(:, 2), t));
	else
		plot3(polyval(p(:, 1), t), polyval(p(:, 2), t), polyval(p(:, 3), t));
	end
end
hold off;
