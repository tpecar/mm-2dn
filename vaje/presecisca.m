function P = presecisca(A, B)
%P = presecisca(A, B) poisce presecisca 
%P = [P1, P2,... , Pm] lomljenk 
%A = [A1, A2, ..., Ak] in B = [B1, B2, ..., Bl].
%(Privzetek: Daljice so v genericni legi - presecisca so transverzalna.) 

%shranimo št. točk na obeh lomljenkah
k = length(A);
l = length(B);
%pripravimo prazen stolpec presecisc P
P = zeros(2, 0);

%poiscemo presecisca vseh parov daljic
for i = 1:(k-1) % zanka po daljicah iz prve lomljenke (A)
	for j = 1:(l-1) % zanka po daljicah iz druge lomljenka (B)
		%Ce se daljici sekata, presecisce dodamo v nabor presecisc P.
		P = [P, presecisce(A(:,[i i+1]), B(:, [j j+1]))];
	end
end